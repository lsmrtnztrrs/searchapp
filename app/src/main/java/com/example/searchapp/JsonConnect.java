package com.example.searchapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class JsonConnect extends AppCompatActivity {

    EditText valueResponseEdit;
    Button button2;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_connect);

        valueResponseEdit = (EditText) findViewById (R.id.urlView);
        button2 = (Button) findViewById(R.id.button2);


        button2.setOnClickListener (new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                JsonConecction connection = new JsonConecction();
                try {
                    String response = connection.execute("http://74.205.41.248:8081/pruebawebservice/api/mob_sp_GetArchivo").get();

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    int code = jsonObject.getInt("code");
                    boolean hasError = jsonObject.getBoolean("HasError");
                    String valueResponse = jsonObject.getString("valueResponse");

                    valueResponseEdit.setText(valueResponse);



                }catch (InterruptedException e){
                    e.printStackTrace();
                }catch (ExecutionException e){
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
